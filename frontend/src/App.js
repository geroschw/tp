import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, Switch, Route, BrowserRouter as Router } from 'react-router-dom';

import { SociosLista } from './componentes/socios/SociosListado';
import { SocioFormulario } from './componentes/socios/SociosFormulario';
import { SociosIndex } from './componentes/socios/SociosIndex'
import { SocioInforme } from './componentes/socios/SocioInforme';
import { DisciplinaLista } from './componentes/disciplinas/DisciplinasListado';
import { DisciplinaFormulario } from './componentes/disciplinas/DisciplinasFormulario';
import { DisciplinasIndex } from './componentes/disciplinas/DisciplinasIndex';
import { ProfesoresLista } from "./componentes/profesores/ProfesoresListado";
import { ProfesorFormulario } from "./componentes/profesores/ProfesoresFormulario";
import { ProfesoresIndex } from "./componentes/profesores/ProfesoresIndex";
import { CategoriasLista } from "./componentes/categorias/CategoriasListado";
import { CategoriaFormulario } from "./componentes/categorias/CategoriasFormulario";
import { CategoriasIndex } from './componentes/categorias/CategoriasIndex';
import { CategoriaSocioFormulario } from "./componentes/categoria_socio/CategoriaSocioFormulario";
import { DisciplinaSocioFormulario } from "./componentes/disciplina_socio/DisciplinaSocioFormulario";
import { DisciplinaSocioLista } from './componentes/disciplina_socio/DisciplinaSocioListado';
import { CoutaFormulario } from './componentes/coutas/CuotasFormulario';
import { CuotasLista } from './componentes/coutas/CuotasListado';
import { CuotasIndex } from './componentes/coutas/CuotasIndex';
import { Inicio } from './componentes/index/Index';


export default function App() {
  return (
    <div style={{ background: "#FCF7FC" }}>
      <Router>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div id="navegacion" className="container-fluid">
            <a className="navbar-brand" to="/"></a>
            <button to="/" className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link  className="nav-link active" aria-current="page" to="/"><span style={{color : "#8BA3EE" }} id="links">INICIO &nbsp;&nbsp;|</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/socios"><span id="links">SOCIOS&nbsp;|</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/profesores"><span id="links">PROFESORES&nbsp;&nbsp;|</span></Link>
                </li>


                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/disciplinas"><span id="links">DISCIPLINAS&nbsp;&nbsp;|</span></Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/categorias"><span id="links">CATEGORÍAS&nbsp;&nbsp;|</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/categoria_socios/1"><span id="links">CATEGORÍAS SOCIOS&nbsp;&nbsp;|</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/disciplina_socio/lista/"><span id="links">DISCIPLINAS SOCIOS&nbsp;&nbsp;|</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/cuotas"><span id="links">CUOTAS&nbsp;</span></Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <Switch>
          <Route path="/socios/lista/:nro" component={SociosLista}></Route>
          <Route path="/socios/info/:nro" component={SocioInforme}></Route>
          <Route path="/socios/lista" component={SociosLista}></Route>
          <Route path="/socios/nuevo" component={SocioFormulario}></Route>
          <Route path="/socios/:nro" component={SocioFormulario}></Route>
          <Route path="/socios" component={SociosIndex}></Route>

          <Route path="/disciplinas/lista/:id" component={DisciplinaLista}></Route>
          <Route path="/disciplinas/lista" component={DisciplinaLista}></Route>
          <Route path="/disciplinas/nuevo" component={DisciplinaFormulario}></Route>
          <Route path="/disciplinas/:id" component={DisciplinaFormulario}></Route>
          <Route path="/disciplinas" component={DisciplinasIndex}></Route>

          <Route path="/profesores/lista/:dni" component={ProfesoresLista}></Route>
          <Route path="/profesores/lista" component={ProfesoresLista}></Route>
          <Route path="/profesores/nuevo" component={ProfesorFormulario}></Route>
          <Route path="/profesores/:dni" component={ProfesorFormulario}></Route>
          <Route path="/profesores" component={ProfesoresIndex}></Route>

          <Route path="/categorias/lista/:id" component={CategoriasLista}></Route>
          <Route path="/categorias/lista" component={CategoriasLista}></Route>
          <Route path="/categorias/nuevo" component={CategoriaFormulario}></Route>
          <Route path="/categorias/:id" component={CategoriaFormulario}></Route>
          <Route path="/categorias" component={CategoriasIndex}></Route>

          <Route path="/categoria_socios/:nro" component={CategoriaSocioFormulario}></Route>

          <Route path="/disciplina_socio/lista/:nro" component={DisciplinaSocioLista}></Route>
          <Route path="/disciplina_socio/lista/" component={DisciplinaSocioLista}></Route>
          <Route path="/disciplina_socio/:nro/:id" component={DisciplinaSocioFormulario}></Route>
          <Route path="/disciplina_socio/:nro" component={DisciplinaSocioFormulario}></Route>

          <Route path="/cuotas/detalle" component={CoutaFormulario}></Route>
          <Route path="/cuotas/informe" component={CuotasLista}></Route>
          <Route path="/cuotas" component={CuotasIndex}></Route>

          <Route path="/" component={Inicio}></Route>

        </Switch>
      </Router>
    </div>
  );
}