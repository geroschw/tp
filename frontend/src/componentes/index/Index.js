import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function Inicio() {
    const [lista, setLista] = useState([])

    useEffect(() => {
        getSocios()
    }, [])

    function getSocios() {
        axios.get("http://127.0.0.1:5000/socios")
            .then((response) => setLista(response.data))
            .catch((error) => alert(error))
    }

    function borrar(id) {
        axios.delete(`http://127.0.0.1:5000/socios/${id}`)
            .then((response) => {
                alert("Registro borrado correctamente")
                getSocios()
            })
            .catch((error) => alert(error))
    }

    return (
        <div >
            <h1>¡Bienvenido al Club!</h1>
            <div style={{ width: "40%", marginLeft:"35%" }}>

                <div className="card text-white bg-danger mb-3" style={{ maxwidth: "18rem;" }}>
                    <div className="card-header">Administración</div>
                    <div className="card-body">
                        <h5 className="card-title">Socios, Profesores, Disciplinas, Categorías</h5>
                        <p className="card-text">Administrar la carga, modificación, eliminación, búsqueda para todas las categorías y ademas la generación de informe de Socio!.</p>
                    </div>
                </div>


                <div className="card text-dark bg-light mb-3" style={{ maxwidth: "18rem;" }}>
                    <div className="card-header">Informes</div>
                    <div className="card-body">
                        <h5 className="card-title">Generación de Cuota</h5>
                        <p className="card-text">Asistente para la generación de la cuota de un determinado Socio para un determinado mes y la posibilidad de registrar el pago.</p>
                    </div>
                </div>
                <div className="card text-white bg-dark mb-3" style={{ maxwidth: "18rem;" }}>
                    <div className="card-header">Informes</div>
                    <div className="card-body">
                        <h5 className="card-title">Detalle de Ingresos</h5>
                        <p className="card-text">Generar un informe en base al monto cobrado y a cobrar por las cuotas de Socios y las listas de socios correspondientes. </p>
                    </div>
                </div>


            </div>
        </div>
    )

}