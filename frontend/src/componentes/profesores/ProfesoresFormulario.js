import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';

export function ProfesorFormulario() {
    const { dni } = useParams()

    const history = useHistory()

    const [profesor, setProfesor] = useState({
        nombre: '',
        dni: '',
        direccion: '',
        telefonos: '',
        titulo: ''
    })

    useEffect(() => {
        if (dni) {
            axios.get(`http://127.0.0.1:5000/profesores/${dni}`)
                .then(response => setProfesor(response.data))
                .catch(error => alert(error))
        }
    }, [])

    function handleOnChange(event, campo) {
        setProfesor({
            ...profesor,
            [campo]: event.target.value
        })
    }

    function guardar(event) {
        event.preventDefault()
        event.stopPropagation()

        if (dni) {
            axios.put(`http://127.0.0.1:5000/profesores/${dni}`, profesor)
                .then(response => {
                    alert("se ha modificado el registro")
                    history.push("/profesores")
                })
                .catch(error => alert(error))
        } else {
            axios.post("http://127.0.0.1:5000/profesores/", profesor)
                .then(response => {
                    alert("se ha agregado el registro")
                    history.push("/profesores")
                })
                .catch(error => alert(error))
        }
    }

    return (
        <div>
            {dni && (<h2>Editando Profesor con el dni {dni}</h2>)}
            {!dni && (<h2>Nuevo Profesor</h2>)}

            <form onSubmit={(event) => guardar(event)}>
                <div className="mb-3">
                    <label className="form-label">Nombre</label>
                    <input className="form-control" aria-describedby="emailHelp" value={profesor.nombre}
                        onChange={(event) => handleOnChange(event, 'nombre')} />

                    <label className="form-label">DNI</label>
                    <input className="form-control" aria-describedby="emailHelp" value={profesor.dni}
                        onChange={(event) => handleOnChange(event, 'dni')} />

                    <label for="exampleInputEmail1" className="form-label">Dirección</label>
                    <input className="form-control" aria-describedby="emailHelp" value={profesor.direccion}
                        onChange={(event) => handleOnChange(event, 'direccion')} />

                    <label className="form-label">Teléfonos</label>
                    <input className="form-control" aria-describedby="emailHelp" value={profesor.telefonos}
                        onChange={(event) => handleOnChange(event, 'telefonos')} />

                    <label className="form-label">Título</label>
                    <input className="form-control" aria-describedby="emailHelp" value={profesor.titulo}
                        onChange={(event) => handleOnChange(event, 'titulo')} />
                </div>

                <button type="submit" className="btn btn-primary">Aceptar</button>
                <button onClick={() => history.push("/profesores")} className="btn btn-primary">Cancelar</button>
            </form>

        </div>
    )
}