import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function ProfesoresLista() {
    const [lista, setLista] = useState([])
    const { dni } = useParams()
    const [profesor, setProfesor] = useState({
        nombre: '',
        dni: '',
        direccion: '',
        telefonos: '',
        titulo: ''
    })
    useEffect(() => {
        getProfesores()
    }, [])

    function getProfesores() {

        if (dni) {
            axios.get(`http://127.0.0.1:5000/profesores/${dni}`)
                .then((response) => setProfesor(response.data))
                .catch((error) => alert(error))
        } else {
            axios.get(`http://127.0.0.1:5000/profesores/`)
                .then((response) => setLista(response.data))
                .catch((error) => alert(error))
        }
    }

    function borrar(id) {
        axios.delete(`http://127.0.0.1:5000/profesores/${id}`)
            .then((response) => {
                alert("Registro borrado correctamente")
                getProfesores()
            })
            .catch((error) => alert(error))
    }

    return (
        <div>
            <h1>Profesores</h1>

            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">DNI</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Teléfonos</th>
                        <th scope="col">Título</th>
                    </tr>
                </thead>
                <tbody>
                    {lista.length > 0 && (
                        lista.map(profesor => (
                            <tr key={profesor.dni}>
                                <td>{profesor.nombre}</td>
                                <td>{profesor.dni}</td>
                                <td>{profesor.direccion}</td>
                                <td>{profesor.telefonos}</td>
                                <td>{profesor.titulo}</td>
                                <td>
                                    <Link to={"/profesores/" + profesor.dni}>Editar</Link> &nbsp;
                                    <span className="btn-link" onClick={() => borrar(profesor.dni)}>Borrar</span>
                                </td>
                            </tr>))
                    )}
                    {lista.length === 0 && (
                        <tr key={profesor.dni}>
                            <td>{profesor.nombre}</td>
                            <td>{profesor.dni}</td>
                            <td>{profesor.direccion}</td>
                            <td>{profesor.telefonos}</td>
                            <td>{profesor.titulo}</td>
                            <td>
                                <Link to={"/profesores/" + profesor.dni}>Editar</Link> &nbsp;
                            <span className="btn-link" onClick={() => borrar(profesor.dni)}>Borrar</span>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
            <Link to={"/profesores/"}>Atrás</Link> &nbsp;
        </div>
    )

}