import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function ProfesoresIndex() {
    const [busqueda, setBusqueda] = useState(1);
    const [dni,setDni] = useState()
    useEffect(() => {

    }, [])



    return (
        <div>
            <h1>Panel de Profesores</h1>


            <ol>
                <li>
                    <Link className="nav-link active" aria-current="page" to="/profesores/nuevo"><span id="jaja">Nuevo Profesor</span></Link>
                </li>
                <li>
                    <Link className="nav-link active" aria-current="page" onClick={() => setBusqueda(busqueda + 1)}><span id="jaja">Buscar Profesor</span></Link>
                </li>




            </ol>
            {busqueda % 2 == 0 && (<div>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="DNI del Profesor" aria-label="Search" 
                    onChange={(event) => setDni(event.target.value)}/>
                    <Link to={"/profesores/lista/"+dni} class="btn btn-outline-success" >Buscar</Link>
                </form> <br/>
                <Link className="nav-link active" aria-current="page" to="/profesores/lista"><span>Lista de Profesores</span></Link>
                
                </div>)}
            <br />
        </div>
    )

}