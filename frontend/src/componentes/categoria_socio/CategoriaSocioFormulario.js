import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function CategoriaSocioFormulario(props) {
    const { nro, informe } = useParams()


    const [socio, setSocio] = useState({
        nro: '',
        nombre: '',
        dni: '',
        direccion: '',
        telefonos: ''
    })
    const history = useHistory()

    const [c_sInforme, setCs] = useState({
        nombre: '',
        fecha_asociacion: '',
        fecha_baja: '',
        nro: '',
        id: ''
    })

    const [listaCategorias, setListaCategorias] = useState([])
    const [categoria_socio, setCatSocio] = useState({
        id: '',
        socio: '',
        categoria: '',
        importe: '',
        fecha_asociacion: '',
        fecha_baja: ''
    })

    const [categoria, setCategoria] = useState({
        id: '',
        nombre: '',
        descripcion: '',
        importe: '',
    })

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/categorias/")
            .then(response => setListaCategorias(response.data))
            .catch(error => alert(error))

        if (nro) {
            axios.get(`http://127.0.0.1:5000/categoria_socio/${nro}`)
                .then(response => setCatSocio(response.data))
                .catch(error => alert(error))
            console.log(nro)
            setCatSocio({
                ...categoria_socio,
                socio: nro
            })
        }
    }, [])

    function handleOnChange(event, campo) {
        setCatSocio({
            ...categoria_socio,
            [campo]: event.target.value
        })
    }

    function actualizarDatosCategoria(event) {
        console.log(event.target.value)
        {
            axios.get(`http://localhost:5000/categorias/${event.target.value}`)
                .then(response => setCategoria(response.data))

                .catch(error => alert(error))

            setCatSocio({
                ...categoria_socio,
                categoria: categoria.id,
                importe: categoria.importe,
            })


        }

    }

    function modificar(event) {
        event.preventDefault()
        event.stopPropagation()
        axios.put(`http://127.0.0.1:5000/categoria_socio/${nro}`, categoria_socio)
            .then(response => {
                alert("se ha modificado el registro")
                //history.push("/socios")
            })
            .catch(error => alert(error))

        console.log(categoria_socio)
    }



    function guardar(event) {
        event.preventDefault()
        event.stopPropagation()
        console.log(categoria_socio)
        axios.post("http://127.0.0.1:5000/categoria_socio/", categoria_socio)
            .then(response => {
                alert("se ha agregado el registro")
            })
            .catch(error => alert(error))
    }


    return (
        <div style={{ margin: "2%", background: "hsl(228, 100%, 93%)", padding: "2%" }}>
            <h2>Categoria de Socio</h2>

            <form >
                <div className="mb-3">

                    {!props.informe && (<div>
                        <label className="form-label">Socio</label>
                        <input className="form-control" aria-describedby="emailHelp" readOnly="True" value={categoria_socio.socio}
                        // onChange={(event) => handleOnChange(event, 'socio')} 
                        />
                        <br /></div>)}


                    <label className="form-label">Categoria</label>
                    <input className="form-control" aria-describedby="emailHelp" readOnly="True" value={categoria_socio.categoria}
                        /*onChange={(event) => handleOnChange(event, 'categoria')} */ />
                    <br />
                    {!props.informe && (<div>
                        <select className="form-select form-select-sm" aria-label=".form-select-sm example"
                            onChange={(event) => actualizarDatosCategoria(event)}>
                            <option defaultValue></option>
                            {listaCategorias.length > 0 && listaCategorias.map(categoriaM => (
                                <option key={categoriaM.id} value={categoriaM.id} >{categoriaM.nombre}</option>
                            ))}

                        </select> <br />

                        <label htmlFor="exampleInputEmail1" className="form-label">Importe</label>
                        <input className="form-control" aria-describedby="emailHelp" readOnly="True" value={categoria_socio.importe}
                            onChange={(event) => handleOnChange(event, 'importe')} />

                        <br /></div>)}

                    <label className="form-label">Fecha Asociacion</label>
                    <input className="form-control" aria-describedby="emailHelp" value={categoria_socio.fecha_asociacion}
                        onChange={(event) => handleOnChange(event, 'fecha_asociacion')} placeholder="YYYY-MM-DD" />

                    <label className="form-label">Fecha Baja</label>
                    <input className="form-control" aria-describedby="emailHelp" value={categoria_socio.fecha_baja}
                        onChange={(event) => handleOnChange(event, 'fecha_baja')} placeholder="YYYY-MM-DD" />
                </div>
                {!props.informe && (<div>
                    <button onClick={(event) => guardar(event)} className="btn btn-primary">Guardar</button>
                    <button onClick={(event) => modificar(event)} className="btn btn-primary">Modificar</button>
                    <br /></div>)}

            </form>
            {!props.informe && (<div>
                <Link to={"/socios/" + nro}>Atrás</Link>
                <br /></div>)}


        </div>
    )
}