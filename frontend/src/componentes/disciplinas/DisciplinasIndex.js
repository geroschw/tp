import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function DisciplinasIndex() {
    const [busqueda, setBusqueda] = useState(1);
    const [id,setId] = useState()
    useEffect(() => {

    }, [])



    return (
        <div>
            <h1>Panel de Disciplinas</h1>


            <ol>
                <li>
                    <Link className="nav-link active" aria-current="page" to="/disciplinas/nuevo"><span id="jaja">Nueva Disciplina</span></Link>
                </li>
                <li>
                    <Link className="nav-link active" aria-current="page" onClick={() => setBusqueda(busqueda + 1)}><span id="jaja">Buscar Disciplina</span></Link>
                </li>




            </ol>
            {busqueda % 2 == 0 && (<div>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="ID de la Disciplina" aria-label="Search" 
                    onChange={(event) => setId(event.target.value)}/>
                    <Link to={"/disciplinas/lista/"+id} class="btn btn-outline-success" >Buscar</Link>
                </form> <br/>
                <Link className="nav-link active" aria-current="page" to="/disciplinas/lista"><span>Lista de Disciplinas</span></Link>
                
                </div>)}
            <br />
        </div>
    )

}