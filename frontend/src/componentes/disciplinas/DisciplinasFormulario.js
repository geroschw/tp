import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';

export function DisciplinaFormulario() {
    const { id } = useParams()

    const history = useHistory()

    const [listaProfesores, setListaProfesores] = useState([])
    const [disciplina, setDisciplina] = useState({
        nombre: '',
        id: '',
        horario: '',
        id_profesor: '',
        fecha_inicio: '',
        fecha_fin: '',
        importe: '',
        porcentaje_profesor: ''
    })
    const [profesor, setProfesor] = useState({
        nombre: '',
        dni: '',
        direccion: '',
        telefonos: '',
        titulo: ''
    })

    useEffect(() => {
        if (id) {
            axios.get(`http://127.0.0.1:5000/disciplinas/${id}`)
                .then(response => setDisciplina(response.data))
                .catch(error => alert(error))
        }
        axios.get("http://127.0.0.1:5000/profesores/")
            .then(response => setListaProfesores(response.data))
            .catch(error => alert(error))
    }, [])

    function handleOnChange(event, campo) {
        setDisciplina({
            ...disciplina,
            [campo]: event.target.value
        })
    }




    function guardar(event) {
        event.preventDefault()
        event.stopPropagation()

        if (id) {
            axios.put(`http://127.0.0.1:5000/disciplinas/${id}`, disciplina)
                .then(response => {
                    alert("se ha modificado el registro")
                    history.push("/disciplinas")
                })
                .catch(error => alert(error))
        } else {
            console.log(disciplina)
            axios.post("http://127.0.0.1:5000/disciplinas/", disciplina)
                .then(response => {
                    alert("se ha agregado el registro")

                    history.push("/disciplinas")
                })
                .catch(error => alert(error))
        }
    }

    return (
        <div>
            {id && (<h2>Editando Disciplina {id}</h2>)}
            {!id && (<h2>Nuevo Disciplina</h2>)}

            <form onSubmit={(event) => guardar(event)}>
                <div className="mb-3">
                    <label className="form-label">Nombre</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina.nombre}
                        onChange={(event) => handleOnChange(event, 'nombre')} />

                    <label className="form-label">Horario</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina.horario}
                        onChange={(event) => handleOnChange(event, 'horario')} />

                    <label className="form-label">Profesor</label>
                    <input className="form-control" readOnly="true" aria-describedby="emailHelp" value={disciplina.id_profesor}
                        onChange={(event) => handleOnChange(event, 'id_profesor')} />
                    <br />
                    <select class="form-select form-select-sm" aria-label=".form-select-sm example"
                        onChange={(event) => handleOnChange(event, 'id_profesor')}>
                        <option selected></option>
                        {listaProfesores.length > 0 && listaProfesores.map(profesorM => (
                            <option key={profesorM.id} value={profesorM.id} >{profesorM.nombre}</option>
                        ))}

                    </select> <br />

                    <label className="form-label">Fecha Inicio</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina.fecha_inicio}
                        onChange={(event) => handleOnChange(event, 'fecha_inicio')} placeholder="YYYY-MM-DD" />

                    <label className="form-label">Fecha Fin</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina.fecha_fin}
                        onChange={(event) => handleOnChange(event, 'fecha_fin')}  placeholder="YYYY-MM-DD"/>

                    <label className="form-label">Importe</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina.importe}
                        onChange={(event) => handleOnChange(event, 'importe')} />

                    <label className="form-label">Porcentaje del Profesor</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina.porcentaje_profesor}
                        onChange={(event) => handleOnChange(event, 'porcentaje_profesor')}  />
                </div>

                <button type="submit" className="btn btn-primary">Aceptar</button>
                <button onClick={() => history.push("/disciplinas")} className="btn btn-primary">Cancelar</button>
            </form>

        </div>
    )
}