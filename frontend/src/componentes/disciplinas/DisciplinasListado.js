import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function DisciplinaLista() {
    const [lista, setLista] = useState([])
    const { id } = useParams()
    const [disciplina, setDisciplina] = useState({
        nombre: '',
        id: '',
        horario: '',
        id_profesor: '',
        fecha_inicio: '',
        fecha_fin: '',
        importe: '',
        porcentaje_profesor: ''
    })
    useEffect(() => {
        getDisciplinas()
    }, [])

    function getDisciplinas() {
        if (id) {
            axios.get(`http://127.0.0.1:5000/disciplinas/${id}`)
                .then((response) => setDisciplina(response.data))
                .catch((error) => alert(error))
        } else {
            axios.get(`http://127.0.0.1:5000/disciplinas/`)
                .then((response) => setLista(response.data))
                .catch((error) => alert(error))
        }
    }

    function borrar(id) {
        axios.delete(`http://127.0.0.1:5000/disciplinas/${id}`)
            .then((response) => {
                alert("Registro borrado correctamente")
                getDisciplinas()
            })
            .catch((error) => alert(error))
    }

    return (
        <div>
            <h1>Disciplinas</h1>

            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Horario</th>
                        <th scope="col">Fecha Inicio</th>
                        <th scope="col">Fecha Final</th>
                        <th scope="col">Importe</th>
                    </tr>
                </thead>
                <tbody>
                {lista.length > 0 && (
                        lista.map(disciplina => (
                            <tr key={disciplina.id}>
                                <td>{disciplina.nombre}</td>
                                <td>{disciplina.horario}</td>
                                <td>{disciplina.fecha_inicio}</td>
                                <td>{disciplina.fecha_fin}</td>
                                <td>{disciplina.importe}</td>
                                <td>
                                    <Link to={"/disciplinas/" + disciplina.id}>Editar</Link> &nbsp;
                                    <span className="btn-link" onClick={() => borrar(disciplina.id)}>Borrar</span>
                                </td>
                            </tr>))
                    )}
                    {lista.length === 0 && (
                        <tr key={disciplina.id}>
                        <td>{disciplina.nombre}</td>
                        <td>{disciplina.horario}</td>
                        <td>{disciplina.fecha_inicio}</td>
                        <td>{disciplina.fecha_fin}</td>
                        <td>{disciplina.importe}</td>
                        <td>
                            <Link to={"/disciplinas/" + disciplina.id}>Editar</Link> &nbsp;
                            <span className="btn-link" onClick={() => borrar(disciplina.id)}>Borrar</span>
                        </td>
                    </tr>
                    )}
                </tbody>
            </table>
            <Link to="/disciplinas/">Atrás</Link>
        </div>
    )

}