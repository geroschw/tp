import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function CuotasLista() {
    const [lista, setLista] = useState([])

    const [couta, setCouta] = useState({

    })

    useEffect(() => {
        getProfesores()
    }, [])

    function getProfesores() {
        axios.get("http://127.0.0.1:5000/socios")
            .then((response) => setLista(response.data))
            .catch((error) => alert(error))
    }


    function actualizarDatosCouta(event, campo) {
        setCouta({
            ...couta,
            [campo]: event.target.value
        })
    }


    return (
        <div>
            <h1>Informe de Cuotas</h1>
            <label className="form-label">Mes</label> <br />

            <select class="form-select form-select-sm" aria-label=".form-select-sm example"
                onChange={(event) => actualizarDatosCouta(event, 'mes')}>
                <option defaultValue></option>
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>

            </select> <br /><br />
            <button className="btn btn-primary">Informe de Cuotas</button>

            <div style={{ margin: "2%", background: "hsl(228, 100%, 93%)", padding: "2%" }}>
                <h2>Socios c/cuotas pagadas</h2>
                <table className="table table-dark table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Nro</th>
                            <th scope="col">Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                        {lista.length > 0 && (
                            lista.map(socio => (
                                <tr key={socio.nro}>
                                    <td>{socio.nro}</td>
                                    <td>{socio.nombre}</td>
                                </tr>))
                        )}
                        {lista.length === 0 && (
                            <tr>
                                <td colSpan="3">
                                    <h2>No hay datos</h2>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
                <label className="form-label">Monto Total por Cuotas Cobradas</label>
                <input readOnly="true" className="form-control" aria-describedby="emailHelp" />
            </div>
            <div style={{margin:"2%",background:"hsl(359, 100%, 95%)",padding:"2%"}}>
                <h2>Socios c/cuotas no pagadas</h2>
                <table className="table table-dark table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Nro</th>
                            <th scope="col">Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                        {lista.length > 0 && (
                            lista.map(profesor => (
                                <tr key={profesor.nro}>
                                    <td>{profesor.nro}</td>
                                    <td>{profesor.nombre}</td>
                                </tr>))
                        )}
                        {lista.length === 0 && (
                            <tr>
                                <td colSpan="3">
                                    <h2>No hay datos</h2>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
                <label className="form-label">Monto Total por Cuotas a Cobrar</label>
                <input readOnly="true" className="form-control" aria-describedby="emailHelp" />
            </div>

            <Link to="/cuotas/">Atrás</Link>
        </div>
    )

}