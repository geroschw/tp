import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { DisciplinaSocioLista } from '../disciplina_socio/DisciplinaSocioListado';

export function CoutaFormulario() {
    const history = useHistory()
    const [pagado,setPagado] = useState(false)
    const [listaSocios, setListaSocios] = useState([])
    const [totalCuota, setTotalCuota] = useState()
    const [calculado, setCalculado] = useState(false)
    const [couta, setCouta] = useState({
        nro: '',
        mes: '',
        importe: ''
    })

    const [listaDisciplinas, setListaDisciplinas] = useState([])



    function getDisciplinasSocio() {
        axios.get(`http://127.0.0.1:5000/disciplinas_socio/${couta.nro}`)
            .then((response) => setListaDisciplinas(response.data))
            .catch((error) => alert(error))
    }

    useEffect(() => {
        axios.get(`http://127.0.0.1:5000/socios/`)
            .then(response => setListaSocios(response.data))
            .catch(error => alert(error))

    }, [])

    function handleOnChange(event, campo) {
        setCouta({
            ...couta,
            [campo]: event.target.value
        })
    }

    function calcular() {
        getDisciplinasSocio()
        setCalculado(true)

        /*calcular el total y asignarlo */
        setTotalCuota(0)
    }

    function pagar() {
        if (!pagado) {
            axios.put(`http://127.0.0.1:5000/cuotas/${couta.nro},${couta.mes}`, true)
                .then(response => {
                    alert("se ha modificado el registro")
                    setPagado(true)
                    history.push("/socios")
                })
                .catch(error => alert(error))

        }
    }

    function actualizarDatosCouta(event, campo) {
        setCouta({
            ...couta,
            [campo]: event.target.value
        })
    }


    /*
        function guardar(event) {
            event.preventDefault()
            event.stopPropagation()
    
            if (nro) {
                axios.put(`http://127.0.0.1:5000/socios/${nro}`, socio)
                    .then(response => {
                        alert("se ha modificado el registro")
                        history.push("/socios")
                    })
                    .catch(error => alert(error))
            } else {
                axios.post("http://127.0.0.1:5000/socios/", socio)
                    .then(response => {
                        alert("se ha agregado el registro")
                        history.push("/socios")
                    })
                    .catch(error => alert(error))
            }
        }*/

    return (
        <div style={{ margin: "2%" }}>
            <h2>Generando Cuota </h2>

            <form /*onSubmit={(event) => guardar(event)}*/>
                <div className="mb-3">
                    <label style={{ marginRight: "11%" }} className="form-label">Socio</label>
                    <label className="form-label">Mes</label> <br />
                    <select style={{ marginRight: "2%" }} class="form-select form-select-sm" aria-label=".form-select-sm example"
                        onChange={(event) => actualizarDatosCouta(event, 'nro')}>
                        <option defaultValue></option>
                        {listaSocios.length > 0 && listaSocios.map(socioM => (
                            <option key={socioM.nro} value={socioM.nro} >{socioM.nombre}</option>
                        ))}

                    </select>

                    <select class="form-select form-select-sm" aria-label=".form-select-sm example"
                        onChange={(event) => actualizarDatosCouta(event, 'mes')}>
                        <option defaultValue></option>
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>

                    </select> <br />

                </div>

                {calculado && (<DisciplinaSocioLista nro={couta.nro} />)}
                <br />
                <label className="form-label">Total Cuota (disciplinas + categoría)</label>
                <input readOnly="true" className="form-control" aria-describedby="emailHelp" value={totalCuota} />

                <br />
                {calculado && (<button onClick={() => pagar()} className="btn btn-danger">Efectuar Pago</button>)}
                <br />
                {!calculado && (<button onClick={() => calcular()} className="btn btn-primary">Cacular Couta</button>)}
                <br /><br />

            </form>
            <Link to="/cuotas/">Atrás</Link>

            {// <button onClick={() => history.push("/categoria_socios/")} className="btn btn-primary">Categoria</button>
            }
        </div>
    )
}