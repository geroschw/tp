import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function CategoriasIndex() {
    const [busqueda, setBusqueda] = useState(1);
    const [id,setId] = useState()
    useEffect(() => {

    }, [])



    return (
        <div>
            <h1>Panel de Categorías</h1>


            <ol>
                <li>
                    <Link className="nav-link active" aria-current="page" to="/categorias/nuevo"><span id="jaja">Nueva Categoría</span></Link>
                </li>
                <li>
                    <Link className="nav-link active" aria-current="page" onClick={() => setBusqueda(busqueda + 1)}><span id="jaja">Buscar Categoría</span></Link>
                </li>




            </ol>
            {busqueda % 2 == 0 && (<div>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="ID de la Categoría" aria-label="Search" 
                    onChange={(event) => setId(event.target.value)}/>
                    <Link to={"/categorias/lista/"+id} class="btn btn-outline-success" >Buscar</Link>
                </form> <br/>
                <Link className="nav-link active" aria-current="page" to="/categorias/lista"><span>Lista de Categorías</span></Link>
                
                </div>)}
            <br />
        </div>
    )

}