import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';

export function CategoriaFormulario() {
    const { id } = useParams()

    const history = useHistory()

    const [categoria, setCategoria] = useState({
        id: '',
        nombre: '',
        descripcion: '',
        importe: '',
    })

    useEffect(() => {
        if (id) {
            axios.get(`http://127.0.0.1:5000/categorias/${id}`)
                .then(response => setCategoria(response.data))
                .catch(error => alert(error))
        }
    }, [])

    function handleOnChange(event, campo) {
        setCategoria({
            ...categoria,
            [campo]: event.target.value
        })
    }

    function guardar(event) {
        event.preventDefault()
        event.stopPropagation()

        if (id) {
            axios.put(`http://127.0.0.1:5000/categorias/${id}`, categoria)
                .then(response => {
                    alert("se ha modificado el registro")
                    history.push("/categorias")
                })
                .catch(error => alert(error))
        } else {
            axios.post("http://127.0.0.1:5000/categorias/", categoria)
                .then(response => {
                    alert("se ha agregado el registro")
                    history.push("/categorias")
                })
                .catch(error => alert(error))
        }
    }

    return (
        <div>
            {id && (<h2>Editando Categoría id {id}</h2>)}
            {!id && (<h2>Nueva Categoría</h2>)}

            <form onSubmit={(event) => guardar(event)}>
                <div className="mb-3">
                    <label className="form-label">Nombre</label>
                    <input className="form-control"aria-describedby="emailHelp" value={categoria.nombre}
                        onChange={(event)=> handleOnChange(event,'nombre')}/>

                    <label className="form-label">Descripción</label>
                    <input className="form-control" aria-describedby="emailHelp" value={categoria.descripcion}
                        onChange={(event)=> handleOnChange(event,'descripcion')}/>
                    
                    <label for="exampleInputEmail1" className="form-label">Importe</label>
                    <input className="form-control" aria-describedby="emailHelp" value={categoria.importe}
                        onChange={(event)=> handleOnChange(event,'importe')}/>
                </div>
                
                <button type="submit" className="btn btn-primary">Aceptar</button>
                <button onClick={() => history.push("/categorias")} className="btn btn-primary">Cancelar</button>
            </form>

        </div>
    )
}