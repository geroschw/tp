import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function CategoriasLista() {
    const [lista, setLista] = useState([])
    const { id } = useParams()
    const [categoria, setCategoria] = useState({
        id: '',
        nombre: '',
        descripcion: '',
        importe: '',
    })
    useEffect(() => {
        getCategorias()
    }, [])

    function getCategorias() {
        if (id) {
            axios.get(`http://127.0.0.1:5000/categorias/${id}`)
                .then((response) => setCategoria(response.data))
                .catch((error) => alert(error))
        } else {
            axios.get(`http://127.0.0.1:5000/categorias/`)
                .then((response) => setLista(response.data))
                .catch((error) => alert(error))
        }
    }

    function borrar(id) {
        axios.delete(`http://127.0.0.1:5000/categorias/${id}`)
            .then((response) => {
                alert("Registro borrado correctamente")
                getCategorias()
            })
            .catch((error) => alert(error))
    }

    return (
        <div>
            <h1>Categorias</h1>

            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Importe</th>
                    </tr>
                </thead>
                <tbody>
                    {lista.length > 0 && (
                        lista.map(categoria => (
                            <tr key={categoria.id}>
                                <td>{categoria.nombre}</td>
                                <td>{categoria.descripcion}</td>
                                <td>{categoria.importe}</td>
                                <td>
                                    <Link to={"/categorias/" + categoria.id}>Editar</Link> &nbsp;
                                    <span className="btn-link" onClick={() => borrar(categoria.id)}>Borrar</span>
                                </td>
                            </tr>))
                    )}
                    {lista.length === 0 && (
                        <tr key={categoria.id}>
                        <td>{categoria.nombre}</td>
                        <td>{categoria.descripcion}</td>
                        <td>{categoria.importe}</td>
                        <td>
                            <Link to={"/categorias/" + categoria.id}>Editar</Link> &nbsp;
                            <span className="btn-link" onClick={() => borrar(categoria.id)}>Borrar</span>
                        </td>
                    </tr>
                    )}
                </tbody>
            </table>
            <Link to="/categorias/">Atrás</Link>
        </div>
    )

}