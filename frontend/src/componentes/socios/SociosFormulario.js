import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { CategoriaSocioFormulario } from '../categoria_socio/CategoriaSocioFormulario';
import {DisciplinaSocioFormulario} from '../disciplina_socio/DisciplinaSocioFormulario'
import { DisciplinaSocioLista } from '../disciplina_socio/DisciplinaSocioListado';

export function SocioFormulario() {
    const { nro } = useParams()
    const history = useHistory()
    const [categoria, setCategoria] = useState(1);
    const [disciplina, setDisciplina] = useState(1);

    const [socio, setSocio] = useState({
        nro: '',
        nombre: '',
        dni: '',
        direccion: '',
        telefonos: ''
    })

    useEffect(() => {
        if (nro) {
            axios.get(`http://127.0.0.1:5000/socios/${nro}`)
                .then(response => setSocio(response.data))
                .catch(error => alert(error))
        }
    }, [])

    function handleOnChange(event, campo) {
        setSocio({
            ...socio,
            [campo]: event.target.value
        })
    }



    function guardar(event) {
        event.preventDefault()
        event.stopPropagation()

        if (nro) {
            axios.put(`http://127.0.0.1:5000/socios/${nro}`, socio)
                .then(response => {
                    alert("se ha modificado el registro")
                    history.push("/socios")
                })
                .catch(error => alert(error))
        } else {
            axios.post("http://127.0.0.1:5000/socios/", socio)
                .then(response => {
                    alert("se ha agregado el registro")
                    history.push("/socios")
                })
                .catch(error => alert(error))
        }
    }

    return (
        <div style={{margin:"2%"}}>
            {nro && (<h2>Editando Socio nro {nro}</h2>)}
            {!nro && (<h2>Nuevo Socio</h2>)}

            <form onSubmit={(event) => guardar(event)}>
                <div className="mb-3">
                    <label className="form-label">Nombre</label>
                    <input className="form-control" aria-describedby="emailHelp" value={socio.nombre}
                        onChange={(event) => handleOnChange(event, 'nombre')} />

                    <label className="form-label">DNI</label>
                    <input className="form-control" aria-describedby="emailHelp" value={socio.dni}
                        onChange={(event) => handleOnChange(event, 'dni')} />

                    <label htmlFor="exampleInputEmail1" className="form-label">Dirección</label>
                    <input className="form-control" aria-describedby="emailHelp" value={socio.direccion}
                        onChange={(event) => handleOnChange(event, 'direccion')} />

                    <label className="form-label">Teléfonos</label>
                    <input className="form-control" aria-describedby="emailHelp" value={socio.telefonos}
                        onChange={(event) => handleOnChange(event, 'telefonos')} />
                </div>

              
                {/*     PERSIANAS CATEGORIAS-DISCIPLINAS
                {categoria % 2 == 0 && (<CategoriaSocioFormulario nro={nro} />)}
                <br />

                {disciplina % 2 == 0 && (<DisciplinaSocioLista nro={nro} />)}
                <br />
                */}

                <button type="submit" className="btn btn-primary">Aceptar</button>

            </form>
            {/*     PERSIANAS CATEGORIAS-DISCIPLINAS
               {nro && (<button onClick={() => setCategoria(categoria + 1)}>
                Categoria
            </button>)}
            <br />
            {nro && (<button onClick={() => setDisciplina(disciplina + 1)}>
                Disciplinas
            </button>)}*/}<br/>
            <Link className="btn btn-danger" to={"/categoria_socios/"+socio.nro}>Categoría</Link> &nbsp;
            <Link className="btn btn-danger" to={"/disciplina_socio/lista/"+socio.nro}>Disciplinas</Link> &nbsp;

            <br/>
            <Link to="/socios/">Atrás</Link>
           {// <button onClick={() => history.push("/categoria_socios/")} className="btn btn-primary">Categoria</button>
           }
        </div>
    )
}