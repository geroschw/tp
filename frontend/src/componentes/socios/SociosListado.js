import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function SociosLista() {
    const [lista, setLista] = useState([])
    const { nro } = useParams()
    const [socio, setSocio] = useState({
        nro: '',
        nombre: '',
        dni: '',
        direccion: '',
        telefonos: ''
    })
    useEffect(() => {
        getSocios()
    }, [])

    function getSocios() {
        if (nro) {
            axios.get(`http://127.0.0.1:5000/socios/${nro}`)
                .then((response) => setSocio(response.data))
                .catch((error) => alert(error))
        } else {
            axios.get(`http://127.0.0.1:5000/socios/`)
                .then((response) => setLista(response.data))
                .catch((error) => alert(error))
        }
    }

    function borrar(id) {
        axios.delete(`http://127.0.0.1:5000/socios/${id}`)
            .then((response) => {
                alert("Registro borrado correctamente")
                getSocios()
            })
            .catch((error) => alert(error))
    }

    return (
        <div>
            <h1>Socios</h1>

            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nro Socio</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">DNI</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Teléfonos</th>
                    </tr>
                </thead>
                <tbody>
                    {lista.length > 0 && (
                        lista.map(socio => (
                            <tr key={socio.nro}>
                                <td>{socio.nro}</td>
                                <td>{socio.nombre}</td>
                                <td>{socio.dni}</td>
                                <td>{socio.direccion}</td>
                                <td>{socio.telefonos}</td>
                                <td>
                                    <Link to={"/socios/" + socio.nro}>Editar</Link> &nbsp;
                                    <span className="btn-link" onClick={() => borrar(socio.nro)}>Borrar</span>

                                </td>
                                <td>
                                    <Link id="jaja" to={"/socios/info/" + socio.nro}>Ver Información</Link> &nbsp;
                                </td>
                            </tr>))
                    )}
                    {lista.length === 0 && (


                        <tr key={socio.nro}>
                            <td>{socio.nro}</td>
                            <td>{socio.nombre}</td>
                            <td>{socio.dni}</td>
                            <td>{socio.direccion}</td>
                            <td>{socio.telefonos}</td>
                            <td>
                                <Link to={"/socios/" + socio.nro}>Editar</Link> &nbsp;
                                    <span className="btn-link" onClick={() => borrar(socio.nro)}>Borrar</span>

                            </td>
                            <td>
                                <Link id="jaja" to={"/socios/info/" + socio.nro}>Ver Información</Link> &nbsp;
                                </td>
                        </tr>


                    )}
                </tbody>
            </table>
            <Link to={"/socios/"}>Atrás</Link> &nbsp;
        </div>
    )

}