import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {SociosLista} from './SociosListado';
import axios from 'axios';

export function SociosIndex() {
    const [busqueda, setBusqueda] = useState(1);
    const [nro,setNro] = useState()

    useEffect(() => {

    }, [])



    return (
        <div>
            <h1>Panel de Socios</h1>


            <ol>
                <li>
                    <Link className="nav-link active" aria-current="page" to="/socios/nuevo"><span id="jaja">Nuevo Socio</span></Link>
                </li>
                <li>
                    <Link className="nav-link active" aria-current="page" onClick={() => setBusqueda(busqueda + 1)}><span id="jaja">Buscar Socio</span></Link>
                </li>

            </ol>
            {busqueda % 2 == 0 && (<div>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Nro de Socio" aria-label="Search" 
                     onChange={(event) => setNro(event.target.value)}/>
                    <Link to={"/socios/lista/"+nro} class="btn btn-outline-success" >Buscar</Link>
                </form> <br/>
                <Link className="nav-link active" aria-current="page" to="/socios/lista"><span>Lista de Socios</span></Link>
                
                </div>)}
            <br />
        </div>
    )

}