import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';

export function DisciplinaSocioFormulario() {
    const { nro, id ,informe } = useParams()
    const history = useHistory()
    const [listaDisciplinas, setListaDisciplinas] = useState([])
    const [socio,setSocio] = useState({
        nro: '',
        nombre: '',
        dni: '',
        direccion: '',
        telefonos: ''
    })

    const [disciplina_socio, setDisciplinaSocio] = useState({
        id: '',
        socio: '',
        disciplina: '',
        fecha_inscripcion: '',
        fecha_baja: ''
    })
    const [disciplina, setDisciplina] = useState({
        nombre: '',
        id: '',
        horario: '',
        id_profesor: '',
        fecha_inicio: '',
        fecha_fin: '',
        importe: '',
        porcentaje_profesor: ''
    })

    useEffect(() => {
        if (nro) {
            axios.get(`http://127.0.0.1:5000/disciplina_socio/${nro}`)
                .then(response => setDisciplinaSocio(response.data))
                .catch(error => alert(error))
        }
        axios.get("http://127.0.0.1:5000/disciplinas/")
            .then(response => setListaDisciplinas(response.data))
            .catch(error => alert(error))

        axios.get(`http://127.0.0.1:5000/socios/${disciplina_socio.socio}`)
            .then(response => setSocio(response.data))
            .catch(error => alert(error))

        setDisciplinaSocio({
            ...disciplina_socio,
            socio:nro
        })

    }, [])

    function handleOnChange(event, campo) {
        setDisciplinaSocio({
            ...disciplina_socio,
            [campo]: event.target.value
        })
    }

    function actualizarDatosDisciplina(event) {
        console.log(event)
        {
            axios.get(`http://localhost:5000/disciplinas/${event.target.value}`)
            .then(response => setDisciplina(response.data))

            .catch(error => alert(error))

            setDisciplinaSocio({
                ...disciplina_socio,
                disciplina: disciplina.id,
            })
        }

    }

    function guardar(event) {
        event.preventDefault()
        event.stopPropagation()
        console.log(disciplina_socio)
        if (id) {

            axios.put(`http://127.0.0.1:5000/disciplina_socio/${id}`, disciplina_socio)
                .then(response => {
                    alert("se ha modificado el registro")
                    //history.push("/socios")
                })
                .catch(error => alert(error))
        } else {
            axios.post("http://127.0.0.1:5000/disciplina_socio/", disciplina_socio)
                .then(response => {
                    alert("se ha agregado el registro")
                    //history.push("/socios")
                })
                .catch(error => alert(error))
        }
    }

    return (
        <div style={{margin:"2%",background:"hsl(359, 100%, 95%)",padding:"2%"}}>
            {id && (<h2>Disciplina de Socio  {id}</h2>)}
            {!id && (<h2>Disciplina de Socio</h2>)}

            <form onSubmit={(event) => guardar(event)}>
                <div className="mb-3">
                    <label className="form-label">Socio</label>
                    <input className="form-control" aria-describedby="emailHelp" readOnly="True" value={disciplina_socio.socio}
                       // onChange={(event) => handleOnChange(event, 'socio')} 
                       />

                    <label className="form-label">Disciplina</label>
                    <input className="form-control" aria-describedby="emailHelp" readOnly="True" value={disciplina_socio.disciplina}
                        onChange={(event) => handleOnChange(event, 'disciplina')} />
                    <br />
                    <select class="form-select form-select-sm" aria-label=".form-select-sm example"
                        onChange={(event) => actualizarDatosDisciplina(event)}>
                        {listaDisciplinas.length > 0 && listaDisciplinas.map(disciplinaM => (
                            <option key={disciplinaM.id} value={disciplinaM.id} >{disciplinaM.nombre}</option>
                        ))}
                        <option defaultValue></option>
                    </select> <br />

                    <label className="form-label">Fecha Inscripcion</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina_socio.fecha_inscripcion}
                        onChange={(event) => handleOnChange(event, 'fecha_inscripcion')} placeholder="YYYY-MM-DD" />

                    <label className="form-label">Fecha Baja</label>
                    <input className="form-control" aria-describedby="emailHelp" value={disciplina_socio.fecha_baja}
                        onChange={(event) => handleOnChange(event, 'fecha_baja')} placeholder="YYYY-MM-DD" />
                </div>

                <button type="submit" className="btn btn-danger">Aceptar</button>
                {//<button onClick={() => history.push("/socios")} className="btn btn-primary">Cancelar</button>
                }
            </form>
            <br/>
            
            <Link to={"/disciplina_socio/lista/" + nro}>Atrás</Link>

        </div>
    )
}