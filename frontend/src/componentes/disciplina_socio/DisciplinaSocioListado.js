import React, { useEffect, useState } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import axios from 'axios';

export function DisciplinaSocioLista(props) {
    const { nro } = useParams()
    const [lista, setLista] = useState([])

    useEffect(() => {
        getDisciplinasSocio()
        console.log(lista)
    }, [])

    function getDisciplinasSocio() {
        if (nro) {
            axios.get(`http://127.0.0.1:5000/disciplinas_socio/${nro}`)
                .then((response) => setLista(response.data))
                .catch((error) => alert(error))
        } else {
            axios.get(`http://127.0.0.1:5000/disciplinas_socio/`)
                .then((response) => setLista(response.data))
                .catch((error) => alert(error))
        }

        
    }

    function borrar(id) {
        axios.delete(`http://127.0.0.1:5000/disciplinas_socio/${id}`)
            .then((response) => {
                alert("Registro borrado correctamente")
                getDisciplinasSocio()
            })
            .catch((error) => alert(error))
    }

    return (
        <div>
            <h1>Disciplinas de Socio</h1>

            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Fecha Inscripcion</th>
                        <th scope="col">Importe</th>
                    </tr>
                </thead>
                <tbody>
                    {lista.length > 0 && (
                        lista.map(disciplina => (
                            <tr key={disciplina.nombre}>
                                <td>{disciplina.nombre}</td>
                                <td>{disciplina.fecha_inscripcion}</td>
                                {/*<td>{disciplina.importe}</td>*/}
                                <td>
                                    <Link to={"/disciplinas/" + disciplina.id}>Editar</Link> &nbsp;
                                    <span className="btn-link" onClick={() => borrar(disciplina.id)}>Borrar</span>
                                </td>
                            </tr>
                            ))
                    )}
                    {lista.length === 0 && (
                        <tr>
                            <td colSpan="3">
                                <h2>No hay datos</h2>
                            </td>
                        </tr>
                    )}
                </tbody>

            </table>
            {!props.informe && (<div>
                <Link to={"/disciplina_socio/" + nro}>Nuevo</Link><br /><br />

                <Link to={"/socios/" + nro}>Atrás</Link>
                <br /></div>)}

        </div>

    )

}