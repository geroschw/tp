from os import environ

from .config_log import config_log


def config_app(app):
    print(f'Usando configuracion de {app.config["ENV"]}')
    config_log(app.name)
    if app.config['ENV'] == 'production':
        app.config.from_object("configuracion.config.ConfigProd")
    if app.config['ENV'] == 'development':
        app.config.from_object("configuracion.config.ConfigDev")
    if app.config['ENV'] == 'testing':
        app.config.from_object("configuracion.config.ConfigTest")


class Config:
    """Configuración base"""
    SECRET_KEY = environ.get('SECRET_KEY')
    SESSION_COOKIE_NAME = environ.get('SESSION_COOKIE_NAME')
    STATIC_FOLDER = 'static'
    TEMPLATES_FOLDER = 'templates'


class ConfigProd(Config):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:postgres@localhost:5432/club"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ConfigDev(Config):
    DEBUG = True
    TESTING = False
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:postgres@localhost:5432/club"#"sqlite:///d:\temp\lab4\test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ConfigTest(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:postgres@localhost:5432/club"
    SQLALCHEMY_TRACK_MODIFICATIONS = False