from logging.config import dictConfig


def config_log(appname):
    config = {
        'version': 1,
        'formatters': {
            'verbose': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'simple'
            },
            'rotating_file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': f'{appname}.log',
                'maxBytes': 10000,
                'backupCount': 3,
                'formatter': 'simple'
            }
        },
        'loggers': {
            'root': {
                'level': 'INFO',
                'handlers': ['rotating_file', 'console']
            },
        }
    }

    config['loggers'][appname] = {
        'level': 'DEBUG',
        'handlers': ['console']
    }

    dictConfig(config)