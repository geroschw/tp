from flask import Flask
from flask_cors import CORS
from flask_restx import Api

from api.socios_api import nsSocios
from api.profesores_api import nsProfesores
from api.disciplinas_api import nsDisciplinas
from api.categorias_api import nsCategorias
from api.categoria_socio_api import nsCategoriaSocio
from api.disciplina_socio_api import nsDisciplinaSocio
from api.cuotas_api import nsCuotas

from configuracion.config import config_app
from datos.manager import create_all
from datos.manager import db

app = Flask(__name__)
config_app(app)
CORS(app)
 
api = Api(app,
        version='1.0',
        title='Administración de club',
        description='-')

api.add_namespace(nsSocios)
api.add_namespace(nsProfesores)
api.add_namespace(nsDisciplinas)
api.add_namespace(nsCategorias)
api.add_namespace(nsCategoriaSocio)
api.add_namespace(nsDisciplinaSocio)
api.add_namespace(nsCuotas)


db.init_app(app)
create_all(app,app.config['SQLALCHEMY_DATABASE_URI'])

if __name__== '__main__':
    print("El servicio está corriendo en la siguiente configuración:")
    print(app.config)
    app.run(port=5000,use_reloader=False)
