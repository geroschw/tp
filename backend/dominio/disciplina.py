from typing import Dict

from datos.manager import db

class Disciplina(db.Model):
    __tablename__ = 'disciplinas'

    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    nombre = db.Column(db.String(80),nullable=False)
    horario = db.Column(db.String(120))
    fecha_inicio = db.Column(db.DateTime)
    fecha_fin = db.Column(db.DateTime)
    importe = db.Column(db.Numeric(5,2))
    id_profesor = db.Column(db.Integer)
    porcentaje_profesor = db.Column(db.Numeric(5,2))


    def as_json(self) -> Dict:
        dictionary = self.__dict__.copy()
        del dictionary['_sa_instance_state']
        return dictionary
