from typing import Dict

from datos.manager import db

class CategoriaSocio(db.Model):
    __tablename__ = 'categorias_socios'

    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    categoria = db.Column(db.Integer,nullable=False)
    socio = db.Column(db.Integer,unique=True)
    importe = db.Column(db.Numeric(5,2))
    fecha_asociacion = db.Column(db.DateTime)
    fecha_baja = db.Column(db.DateTime)

    def as_json(self) -> Dict:
        dictionary = self.__dict__.copy()
        del dictionary['_sa_instance_state']
        return dictionary
