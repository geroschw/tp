from typing import Dict

from datos.manager import db

class Categoria(db.Model):
    __tablename__ = 'categorias'

    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    nombre = db.Column(db.String(80),nullable=False)
    descripcion = db.Column(db.String(120))
    importe = db.Column(db.Numeric(5,2))

    def as_json(self) -> Dict:
        dictionary = self.__dict__.copy()
        del dictionary['_sa_instance_state']
        return dictionary
