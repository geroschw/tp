from typing import Dict

from datos.manager import db

class Cuota(db.Model):
    __tablename__ = 'cuotas'

    nro = db.Column(db.Integer,primary_key=True)
    enero = db.Column(db.Boolean)
    febrero = db.Column(db.Boolean)
    marzo = db.Column(db.Boolean)
    abril = db.Column(db.Boolean)
    mayo = db.Column(db.Boolean)
    junio = db.Column(db.Boolean)
    julio = db.Column(db.Boolean)
    agosto = db.Column(db.Boolean)
    septiembre = db.Column(db.Boolean)
    octubre = db.Column(db.Boolean)
    noviembre = db.Column(db.Boolean)
    diciembre = db.Column(db.Boolean)

    def as_json(self) -> Dict:
        dictionary = self.__dict__.copy()
        del dictionary['_sa_instance_state']
        return dictionary
