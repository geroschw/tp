from typing import Dict

from datos.manager import db

class DisciplinaSocio(db.Model):
    __tablename__ = 'disciplinas_socios'

    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    disciplina = db.Column(db.Integer,nullable=False)
    socio = db.Column(db.Integer)
    fecha_inscripcion = db.Column(db.DateTime)
    fecha_baja = db.Column(db.DateTime)

    def as_json(self) -> Dict:
        dictionary = self.__dict__.copy()
        del dictionary['_sa_instance_state']
        return dictionary
