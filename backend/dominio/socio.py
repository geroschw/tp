from typing import Dict

from datos.manager import db

class Socio(db.Model):
    __tablename__ = 'socios'

    nro = db.Column(db.Integer,primary_key=True,autoincrement=True)
    nombre = db.Column(db.String(80),nullable=False)
    dni = db.Column(db.Integer)
    direccion = db.Column(db.String(120))
    telefonos = db.Column(db.String(80))

    def as_json(self) -> Dict:
        dictionary = self.__dict__.copy()
        del dictionary['_sa_instance_state']
        return dictionary
