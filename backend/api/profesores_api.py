from flask import Flask
from flask_restx import Namespace,Resource

from api.utils import ModeloProfesor,parserProfesor,ModeloDNIProfesor,parserEditarProfesor
from dominio.profesor import Profesor
from infraestructura.profesores_repo_sql import ProfesoresRepoSql

nsProfesores = Namespace(
    'profesores',description='Administración de profesores',path='/profesores')

app= Flask(__name__)
repo = ProfesoresRepoSql()

@nsProfesores.route('/')
class ProfesoresResource(Resource):
    @nsProfesores.marshal_list_with(ModeloProfesor)
    def get(self):
        return repo.get_all()

    @nsProfesores.marshal_with(ModeloProfesor)
    @nsProfesores.expect(ModeloProfesor)
    def post(self):
        data = parserProfesor.parse_args()
        c = Profesor(nombre=data.get('nombre'),dni=data.get('dni'),direccion=data.get('direccion'),telefonos=data.get('telefonos'),titulo=data.get('titulo'))
        repo.agregar(c)
        return c,201

@nsProfesores.route('/<int:id>')
class ProfesorResource(Resource):
    @nsProfesores.expect(ModeloDNIProfesor)
    @nsProfesores.marshal_with(ModeloProfesor)
    @nsProfesores.param('<int:id>')
    def get(self,id):
        print(id)
        c = repo.buscar_profesor(id)
        if c:
            return c,200
        return f'Profesor (dni: {id}) no encontrado',404

    @nsProfesores.param('<int:id>')
    @nsProfesores.expect(ModeloDNIProfesor)
    def put(self,id):
        data = parserEditarProfesor.parse_args()
        repo.modificar(id,data)
        return f'Profesor (dni: {id}) modificado correctamente',200

    @nsProfesores.param('<int:id>')
    @nsProfesores.expect(ModeloDNIProfesor)
    def delete(self,id):
        repo.borrar(id)
        return f'Profesor (dni: {id}) eliminado correctamente',200