from flask import Flask
from flask_restx import Namespace,Resource

from api.utils import ModeloDisciplina,parserDisciplina,ModeloIdDisciplina,parserEditarDisciplina
from dominio.disciplina import Disciplina
from infraestructura.disciplinas_repo_sql import DisciplinasRepoSql

nsDisciplinas = Namespace(
    'disciplinas',description='Administración de disciplinas',path='/disciplinas')

app= Flask(__name__)
repo = DisciplinasRepoSql()

@nsDisciplinas.route('/')
class DisciplinasResource(Resource):
    @nsDisciplinas.marshal_list_with(ModeloDisciplina)
    def get(self):
        return repo.get_all()

    @nsDisciplinas.marshal_with(ModeloDisciplina)
    @nsDisciplinas.expect(ModeloDisciplina)
    def post(self):
        print("hola")
        data = parserDisciplina.parse_args()
        c = Disciplina(nombre=data.get('nombre'),horario=data.get('horario'),fecha_inicio=data.get('fecha_inicio'),fecha_fin=data.get('fecha_fin'),importe=data.get('importe'),id_profesor=data.get('id_profesor'),porcentaje_profesor=data.get('porcentaje_profesor'))
        
        repo.agregar(c)
        return c,201

@nsDisciplinas.route('/<int:id>')
class DisciplinaResource(Resource):
    @nsDisciplinas.expect(ModeloIdDisciplina)
    @nsDisciplinas.marshal_with(ModeloDisciplina)
    @nsDisciplinas.param('<int:id>')
    def get(self,id):
        print(id)
        c = repo.buscar_disciplina(id)
        if c:
            return c,200
        return f'Disciplina (id: {id}) no encontrado',404

    @nsDisciplinas.param('<int:id>')
    @nsDisciplinas.expect(ModeloIdDisciplina)
    def put(self,id):
        data = parserEditarDisciplina.parse_args()
        repo.modificar(id,data)
        return f'Disciplina (id: {id}) modificado correctamente',200

    @nsDisciplinas.param('<int:id>')
    @nsDisciplinas.expect(ModeloIdDisciplina)
    def delete(self,id):
        repo.borrar(id)
        return f'Disciplina (id: {id}) eliminado correctamente',200