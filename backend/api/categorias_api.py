from flask import Flask
from flask_restx import Namespace,Resource

from api.utils import ModeloCategoria,parserCategoria,ModeloIdCategoria,parserEditarCategoria
from dominio.categoria import Categoria
from infraestructura.categorias_repo_sql import CategoriasRepoSql

nsCategorias = Namespace(
    'categorias',description='Administración de categorias',path='/categorias')

app= Flask(__name__)
repo = CategoriasRepoSql()

@nsCategorias.route('/')
class CategoriasResource(Resource):
    @nsCategorias.marshal_list_with(ModeloCategoria)
    def get(self):
        return repo.get_all()

    @nsCategorias.marshal_with(ModeloCategoria)
    @nsCategorias.expect(ModeloCategoria)
    def post(self):
        data = parserCategoria.parse_args()
        c = Categoria(nombre=data.get('nombre'),descripcion=data.get('descripcion'),importe=data.get('importe'))
        repo.agregar(c)
        return c,201

@nsCategorias.route('/<int:id>')
class CategoriaResource(Resource):
    @nsCategorias.expect(ModeloIdCategoria)
    @nsCategorias.marshal_with(ModeloCategoria)
    @nsCategorias.param('<int:id>')
    def get(self,id):
        print(id)
        c = repo.buscar_categoria(id)
        if c:
            return c,200
        return f'Categoria (nro: {id}) no encontrado',404

    @nsCategorias.param('<int:id>')
    @nsCategorias.expect(ModeloIdCategoria)
    def put(self,id):
        data = parserEditarCategoria.parse_args()
        repo.modificar(id,data)
        return f'Categoria (nro: {id}) modificado correctamente',200

    @nsCategorias.param('<int:id>')
    @nsCategorias.expect(ModeloIdCategoria)
    def delete(self,id):
        repo.borrar(id)
        return f'Categoria (nro: {id}) eliminado correctamente',200