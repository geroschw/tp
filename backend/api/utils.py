from flask_restx import Model,fields,reqparse

ModeloSocio = Model('Socio',{
    'nombre':fields.String(required=True),
    'dni':fields.Integer(required=True),
    'direccion':fields.String,
    'telefonos':fields.String,
    'nro':fields.Integer(required=True)
})

ModeloNroSocio = Model('Socio',{
    'nro':fields.Integer(required=True)
})

parserSocio = reqparse.RequestParser(bundle_errors=True)
parserSocio.add_argument('nombre',type=str,required=True,location='json')
parserSocio.add_argument('direccion',type=str)
parserSocio.add_argument('telefonos',type=str)
parserSocio.add_argument('dni',type=int)

parserEditarSocio = parserSocio.copy()
parserEditarSocio.add_argument(
    'nro',type=int,required=True,location='json')

def row2dict(row):
    return{
        c.name: str(getattr(row,c.name))
        for c in row.__table__.columns
    }

class CustomField(fields.Raw):
    def format(self,value):
        if isinstance(value,list):
            return [(v.as_dict()) for v in value]
        else:
            return value.as_json()


ModeloProfesor = Model('Profesor',{
    'nombre':fields.String(required=True),
    'dni':fields.Integer(required=True),
    'direccion':fields.String,
    'telefonos':fields.String,
    'titulo':fields.String
})

ModeloDNIProfesor = Model('Profesor',{
    'dni':fields.Integer(required=True)
})

parserProfesor =  reqparse.RequestParser(bundle_errors=True)
parserProfesor.add_argument('nombre',type=str,required=True,location='json')
parserProfesor.add_argument('direccion',type=str)
parserProfesor.add_argument('telefonos',type=str)
parserProfesor.add_argument('dni',type=int,required=True)
parserProfesor.add_argument('titulo',type=str)

parserEditarProfesor = parserProfesor.copy()
parserEditarProfesor.add_argument(
    'dni',type=int,required=True,location='json')


ModeloDisciplina = Model('Disciplina',{
    'id':fields.Integer,
    'nombre':fields.String(required=True),
    'horario':fields.String(required=True),
    'fecha_inicio':fields.DateTime(required=True),
    'fecha_fin':fields.DateTime(required=True),
    'importe':fields.Float(required=True),
    'id_profesor':fields.Integer(required=True),
    'porcentaje_profesor':fields.Float(required=True)
})

ModeloIdDisciplina = Model('Disciplina',{
    'id':fields.Integer(required=True)
})

parserDisciplina =  reqparse.RequestParser(bundle_errors=True)
#parserDisciplina.add_argument('id',type=int)
parserDisciplina.add_argument('nombre',type=str,required=True,location='json')
parserDisciplina.add_argument('horario',type=str)
parserDisciplina.add_argument('fecha_inicio',type=str)
parserDisciplina.add_argument('fecha_fin',type=str)
parserDisciplina.add_argument('importe',type=float)
parserDisciplina.add_argument('id_profesor',type=int)
parserDisciplina.add_argument('porcentaje_profesor',type=float)

parserEditarDisciplina = parserDisciplina.copy()
parserEditarDisciplina.add_argument(
    'id',type=int,required=True,location='json')


ModeloCategoria = Model('Categoria',{
    'id':fields.Integer(required=True),
    'nombre':fields.String(required=True),
    'descripcion':fields.String,
    'importe':fields.Float
})

ModeloIdCategoria = Model('Categoria',{
    'id':fields.Integer(required=True)
})

parserCategoria =  reqparse.RequestParser(bundle_errors=True)
parserCategoria.add_argument('nombre',type=str,required=True,location='json')
parserCategoria.add_argument('descripcion',type=str)
parserCategoria.add_argument('importe',type=float)

parserEditarCategoria = parserCategoria.copy()
parserEditarCategoria.add_argument(
    'id',type=int,required=True,location='json')


ModeloCategoriaSocio = Model('CategoriaSocio',{
    'id':fields.Integer(required=True),
    'categoria':fields.Integer(required=True),
    'socio':fields.Integer(required=True),
    'importe':fields.Float,
    'fecha_asociacion':fields.DateTime,
    'fecha_baja':fields.DateTime
})

ModeloIdCategoriaSocio = Model('CategoriaSocio',{
    'socio':fields.Integer(required=True)
})

parserCategoriaSocio =  reqparse.RequestParser(bundle_errors=True)
parserCategoriaSocio.add_argument('id',type=int,required=True,location='json')
parserCategoriaSocio.add_argument('categoria',type=int,required=True)
parserCategoriaSocio.add_argument('importe',type=float)
parserCategoriaSocio.add_argument('fecha_asociacion',type=str)
parserCategoriaSocio.add_argument('fecha_baja',type=str)

parserEditarCategoriaSocio = parserCategoria.copy()
parserEditarCategoriaSocio.add_argument(
    'socio',type=int,required=True,location='json')


ModeloDisciplinaSocio = Model('DisciplinaSocio',{
    'id':fields.Integer(required=True),
    'disciplina':fields.Integer(required=True),
    'socio':fields.Integer(required=True),
    'fecha_inscripcion':fields.DateTime,
    'fecha_baja':fields.DateTime 
})

ModeloIdDisciplinaSocio = Model('DisciplinaSocio',{
    'id':fields.Integer(required=True)
})

parserDisciplinaSocio =  reqparse.RequestParser(bundle_errors=True)
parserDisciplinaSocio.add_argument('id',type=int,required=True,location='json')
parserDisciplinaSocio.add_argument('disciplina',type=int,required=True)
parserDisciplinaSocio.add_argument('socio',type=int,required=True)
parserDisciplinaSocio.add_argument('fecha_inscripcion',type=str)
parserDisciplinaSocio.add_argument('fecha_baja',type=str)

parserEditarDisciplinaSocio = parserCategoria.copy()
parserEditarDisciplinaSocio.add_argument(
    'id',type=int,required=True,location='json')


ModeloCuota = Model('Cuota',{
    'nro':fields.Integer(required=True),
    'enero':fields.Boolean,
    'febrero':fields.Boolean,
    'marzo':fields.Boolean,
    'abril':fields.Boolean,
    'mayo':fields.Boolean,
    'junio':fields.Boolean,
    'julio':fields.Boolean,
    'agosto':fields.Boolean,
    'septiembre':fields.Boolean,
    'octubre':fields.Boolean,
    'noviembre':fields.Boolean,
    'diciembre':fields.Boolean
})

ModeloNroCuota = Model('Cuota',{
    'nro':fields.Integer(required=True)
})

parserCuota =  reqparse.RequestParser(bundle_errors=True)
parserCuota.add_argument('enero',type=bool,required=True,location='json')
parserCuota.add_argument('febrero',type=bool)
parserCuota.add_argument('marzo',type=bool)
parserCuota.add_argument('abril',type=bool)
parserCuota.add_argument('mayo',type=bool)
parserCuota.add_argument('junio',type=bool)
parserCuota.add_argument('julio',type=bool)
parserCuota.add_argument('agosto',type=bool)
parserCuota.add_argument('septiembre',type=bool)
parserCuota.add_argument('octubre',type=bool)
parserCuota.add_argument('noviembre',type=bool)
parserCuota.add_argument('diciembre',type=bool)

parserEditarCuota = parserCuota.copy()
parserEditarCuota.add_argument(
    'nro',type=int,required=True,location='json')
