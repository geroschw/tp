from flask import Flask
from flask_restx import Namespace,Resource

from api.utils import ModeloCuota,parserCuota,ModeloNroCuota,parserEditarCuota
from dominio.cuotas import Cuota
from infraestructura.cuotas_repo_sql import CuotasRepoSql

nsCuotas = Namespace(
    'cuotas',description='Administración de cuotas',path='/cuotas')

app= Flask(__name__)
repo = CuotasRepoSql()

@nsCuotas.route('/')
class CuotasResource(Resource):
    @nsCuotas.marshal_list_with(ModeloCuota)
    def get(self):
        return repo.get_all()

    @nsCuotas.marshal_with(ModeloCuota)
    @nsCuotas.expect(ModeloCuota)
    def post(self):
        data = parserCuota.parse_args()
        #c = Cuota(enero=data.get('enero'),descripcion=data.get('descripcion'),importe=data.get('importe'))
        c = Cuota(False,False,False,False,False,False,False,False,False,False,False,False)
        repo.agregar(c)
        return c,201

@nsCuotas.route('/<int:id>')
class CuotaResource(Resource):
    @nsCuotas.expect(ModeloNroCuota)
    @nsCuotas.marshal_with(ModeloCuota)
    @nsCuotas.param('<int:id>')
    def get(self,id):
        print(id)
        c = repo.buscar_cuota(id)
        if c:
            return c,200
        return f'Cuota (nro: {id}) no encontrada',404

    @nsCuotas.param('<int:id>')
    @nsCuotas.expect(ModeloNroCuota)
    def put(self,id):
        data = parserEditarCuota.parse_args()
        repo.modificar(id,data)
        return f'Cuota (nro: {id}) modificado correctamente',200

    @nsCuotas.param('<int:id>')
    @nsCuotas.expect(ModeloNroCuota)
    def delete(self,id):
        repo.borrar(id)
        return f'Cuota (nro: {id}) eliminada correctamente',200