from flask import Flask
from flask_restx import Namespace,Resource

from api.utils import ModeloDisciplinaSocio,parserDisciplinaSocio,ModeloIdDisciplinaSocio,parserEditarDisciplinaSocio
from dominio.disciplina_socio import DisciplinaSocio
from infraestructura.disciplinas_socios_repo_sql import DisciplinasSociosRepoSql

nsDisciplinaSocio = Namespace(
    'disciplinasSocios',description='Administración de disciplinas de socios',path='/disciplina_socio')

app= Flask(__name__)
repo = DisciplinasSociosRepoSql()

@nsDisciplinaSocio.route('/')
class DisciplinasSociosResource(Resource):
    @nsDisciplinaSocio.marshal_list_with(ModeloDisciplinaSocio)
    def get(self):
        return repo.get_all()

    @nsDisciplinaSocio.marshal_with(ModeloDisciplinaSocio)
    @nsDisciplinaSocio.expect(ModeloDisciplinaSocio)
    def post(self):
        data = parserDisciplinaSocio.parse_args()
        c = DisciplinaSocio(disciplina=data.get('disciplina'),socio=data.get('socio'),fecha_inscripcion=data.get('fecha_inscripcion'),fecha_baja=data.get('fecha_baja'))
        repo.agregar(c)
        return c,201

@nsDisciplinaSocio.route('/<int:id>')
class DisciplinaSocioResource(Resource):
    @nsDisciplinaSocio.expect(ModeloIdDisciplinaSocio)
    @nsDisciplinaSocio.marshal_with(ModeloDisciplinaSocio)
    @nsDisciplinaSocio.param('<int:id>')
    def get(self,id):
        print(id)
        return repo.get(id)

    @nsDisciplinaSocio.param('<int:id>')
    @nsDisciplinaSocio.expect(ModeloIdDisciplinaSocio)
    def put(self,id):
        data = parserEditarDisciplinaSocio.parse_args()
        repo.modificar(id,data)
        return f'Categoria socio (nro: {id}) modificado correctamente',200

    @nsDisciplinaSocio.param('<int:id>')
    @nsDisciplinaSocio.expect(ModeloIdDisciplinaSocio)
    def delete(self,id):
        repo.borrar(id)
        return f'Categoria socio (nro: {id}) eliminado correctamente',200