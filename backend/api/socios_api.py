from flask import Flask
from flask_restx import Namespace,Resource

from api.utils import ModeloSocio,parserSocio,ModeloNroSocio,parserEditarSocio
from dominio.socio import Socio
from infraestructura.socios_repo_sql import SociosRepoSql

nsSocios = Namespace(
    'socios',description='Administración de socios',path='/socios')

app= Flask(__name__)
repo = SociosRepoSql()

@nsSocios.route('/')
class SociosResource(Resource):
    @nsSocios.marshal_list_with(ModeloSocio)
    def get(self):
        return repo.get_all()

    @nsSocios.marshal_with(ModeloSocio)
    @nsSocios.expect(ModeloSocio)
    def post(self):
        data = parserSocio.parse_args()
        c = Socio(nombre=data.get('nombre'),dni=data.get('dni'),direccion=data.get('direccion'),telefonos=data.get('telefonos'))
        repo.agregar(c)
        return c,201

@nsSocios.route('/<int:id>')
class SocioResource(Resource):
    @nsSocios.expect(ModeloNroSocio)
    @nsSocios.marshal_with(ModeloSocio)
    @nsSocios.param('<int:id>')
    def get(self,id):
        print(id)
        c = repo.buscar_socio(id)
        if c:
            return c,200
        return f'Socio (nro: {id}) no encontrado',404

    @nsSocios.param('<int:id>')
    @nsSocios.expect(ModeloNroSocio)
    def put(self,id):
        data = parserEditarSocio.parse_args()
        repo.modificar(id,data)
        return f'Socio (nro: {id}) modificado correctamente',200

    @nsSocios.param('<int:id>')
    @nsSocios.expect(ModeloNroSocio)
    def delete(self,id):
        repo.borrar(id)
        return f'Socio (nro: {id}) eliminado correctamente',200