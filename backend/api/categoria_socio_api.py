from flask import Flask
from flask_restx import Namespace,Resource

from api.utils import ModeloCategoriaSocio,parserCategoriaSocio,ModeloIdCategoriaSocio,parserEditarCategoriaSocio
from dominio.categoria_socio import CategoriaSocio
from infraestructura.categorias_socios_repo_sql import CategoriasSociosRepoSql

nsCategoriaSocio = Namespace(
    'categoriasSocios',description='Administración de categorias de socios',path='/categoria_socio')

app= Flask(__name__)
repo = CategoriasSociosRepoSql()

@nsCategoriaSocio.route('/')
class CategoriasSociosResource(Resource):
    @nsCategoriaSocio.marshal_list_with(ModeloCategoriaSocio)
    def get(self):
        return repo.get_all()

    @nsCategoriaSocio.marshal_with(ModeloCategoriaSocio)
    @nsCategoriaSocio.expect(ModeloCategoriaSocio)
    def post(self):
        data = parserCategoriaSocio.parse_args()
        c = CategoriaSocio(categoria=data.get('categoria'),socio=data.get('socio'),importe=data.get('importe'),fecha_asociacion=data.get('fecha_asociacion'),fecha_baja=data.get('fecha_baja'))
        repo.agregar(c)
        return c,201

@nsCategoriaSocio.route('/<int:socio>')
class CategoriaSocioResource(Resource):
    @nsCategoriaSocio.expect(ModeloIdCategoriaSocio)
    @nsCategoriaSocio.marshal_with(ModeloCategoriaSocio)
    @nsCategoriaSocio.param('<int:socio>')
    def get(self,socio):
        print(id)
        return repo.get_catSocio(socio)

    @nsCategoriaSocio.param('<int:socio>')
    @nsCategoriaSocio.expect(ModeloIdCategoriaSocio)
    def put(self,socio):
        data = parserEditarCategoriaSocio.parse_args()
        repo.modificar(socio,data)
        return f'Categoria socio (nro: {socio}) modificado correctamente',200

    @nsCategoriaSocio.param('<int:id>')
    @nsCategoriaSocio.expect(ModeloIdCategoriaSocio)
    def delete(self,id):
        repo.borrar(id)
        return f'Categoria socio (nro: {id}) eliminado correctamente',200