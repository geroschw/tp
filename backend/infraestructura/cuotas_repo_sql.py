from typing import List

from datos.manager import db
from dominio.cuotas import Cuota

class CuotasRepoSql:
    def get_all(self) -> List[Cuota]:
        return Cuota.query.all()

    def agregar(self,cuota):
        db.session.add(cuota)
        db.session.commit()

    def modificar(self,nro,jsonData) -> bool:
        modified = Cuota.query.filter_by(nro=nro).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,nro) -> bool:
        old = self.buscar_cuota(nro)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_cuota(self,nro) -> Cuota:
        return Cuota.query.get(id)