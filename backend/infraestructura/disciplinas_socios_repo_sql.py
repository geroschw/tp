from typing import List
from sqlalchemy.sql import text

from datos.manager import db
from dominio.disciplina_socio import DisciplinaSocio

class DisciplinasSociosRepoSql:
    def get_all(self) -> List[DisciplinaSocio]:
        return DisciplinaSocio.query.all()

    def get(self,id) -> List[DisciplinaSocio]:
        return db.session.query().from_statement(text("select * from (select d.nombre,ds.fecha_inscripcion,d.importe,s.nro from socios s join disciplinas_socios ds on s.nro=ds.socio join disciplinas d on d.id=ds.id) as r where nro=IDSOCIO")).params(IDSOCIO=id).all() 

    def agregar(self,disciplinaSocio):
        db.session.add(disciplinaSocio)
        db.session.commit()

    def modificar(self,id,jsonData) -> bool:
        modified = DisciplinaSocio.query.filter_by(id=id).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,id) -> bool:
        old = self.buscar_disciplinaSocio(id)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_disciplinaSocio(self,id) -> DisciplinaSocio:
        return DisciplinaSocio.query.get(id)