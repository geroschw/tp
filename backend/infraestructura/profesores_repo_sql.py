from typing import List

from datos.manager import db
from dominio.profesor import Profesor

class ProfesoresRepoSql:
    def get_all(self) -> List[Profesor]:
        return Profesor.query.all()

    def agregar(self,profesor):
        db.session.add(profesor)
        db.session.commit()

    def modificar(self,dni,jsonData) -> bool:
        modified = Profesor.query.filter_by(dni=dni).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,dni) -> bool:
        old = self.buscar_profesor(dni)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_profesor(self,dni) -> Profesor:
        return Profesor.query.get(dni)