from typing import List

from datos.manager import db
from dominio.categoria import Categoria

class CategoriasRepoSql:
    def get_all(self) -> List[Categoria]:
        return Categoria.query.all()

    def agregar(self,categoria):
        db.session.add(categoria)
        db.session.commit()

    def modificar(self,id,jsonData) -> bool:
        modified = Categoria.query.filter_by(id=id).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,id) -> bool:
        old = self.buscar_categoria(id)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_categoria(self,id) -> Categoria:
        return Categoria.query.get(id)