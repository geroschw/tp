from typing import List

from datos.manager import db
from dominio.socio import Socio

class SociosRepoSql:
    def get_all(self) -> List[Socio]:
        return Socio.query.all()

    def agregar(self,socio):
        db.session.add(socio)
        db.session.commit()

    def modificar(self,nro,jsonData) -> bool:
        modified = Socio.query.filter_by(nro=nro).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,nro) -> bool:
        old = self.buscar_socio(nro)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_socio(self,nro) -> Socio:
        return Socio.query.get(nro)