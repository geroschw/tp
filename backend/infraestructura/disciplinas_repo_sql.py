from typing import List

from datos.manager import db
from dominio.disciplina import Disciplina

class DisciplinasRepoSql:
    def get_all(self) -> List[Disciplina]:
        return Disciplina.query.all()

    def agregar(self,disciplina):
        db.session.add(disciplina)
        db.session.commit()

    def modificar(self,id,jsonData) -> bool:
        modified = Disciplina.query.filter_by(id=id).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,id) -> bool:
        old = self.buscar_disciplina(id)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_disciplina(self,id) -> Disciplina:
        return Disciplina.query.get(id)