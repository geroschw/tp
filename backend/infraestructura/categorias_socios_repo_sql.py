from typing import List
from sqlalchemy.sql import text

from datos.manager import db
from dominio.categoria_socio import CategoriaSocio

class CategoriasSociosRepoSql:
    def get_all(self) -> List[CategoriaSocio]:
        return CategoriaSocio.query.all()

    def get(self,id) -> CategoriaSocio:
        return db.session.query().from_statement(text("select * from (select c.nombre,cs.fecha_asociacion,cs.fecha_baja,s.nro from socios s join  categorias_socios cs on s.nro=cs.socio join categorias c on cs.categoria=c.id) as r where nro=:IDSOCIO")).params(IDSOCIO=id).all() 

    def get_catSocio(self,socio) ->CategoriaSocio:
        return db.session.query().from_statement(text("select * from categorias_socios where socio =:socio")).params(socio=socio).all() 

    def agregar(self,categoriaSocio):
        db.session.add(categoriaSocio)
        db.session.commit()

    def modificar(self,socio,jsonData) -> bool:
        modified = CategoriaSocio.query.filter_by(socio=socio).update(jsonData)

        db.session.commit()
        return modified > 0

    def borrar(self,id) -> bool:
        old = self.buscar_categoriaSocio(id)
        if not old:
            return False
        db.session.delete(old)
        db.session.commit()
        return True

    def buscar_categoriaSocio(self,id) -> CategoriaSocio:
        return CategoriaSocio.query.get(id)